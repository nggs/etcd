package etcdv3

import (
	"context"
	"sync"
	"time"

	"github.com/coreos/etcd/clientv3"
)

const (
	dialTimeout    = 3 * time.Second
	requestTimeout = 3 * time.Second
)

var SC = NewSimpleClient()

type SimpleClient struct {
	*clientv3.Client
}

func NewSimpleClient() *SimpleClient {
	return &SimpleClient{}
}

func (sc *SimpleClient) Init(endpoint string) (err error) {
	sc.Client, err = clientv3.New(clientv3.Config{
		Endpoints:   []string{endpoint},
		DialTimeout: dialTimeout,
	})
	if err != nil {
		return
	}
	return
}

func (sc *SimpleClient) GrantLease(timeout int64) (clientv3.LeaseID, error) {
	resp, err := sc.Grant(context.TODO(), timeout)
	if err != nil {
		return 0, err
	}
	return resp.ID, nil
}

func (sc *SimpleClient) KeepAliveLease(leaseID clientv3.LeaseID) (<-chan *clientv3.LeaseKeepAliveResponse, error) {
	rch, err := sc.KeepAlive(context.TODO(), leaseID)
	return rch, err
}

func (sc *SimpleClient) put(key string, value string, opts ...clientv3.OpOption) (*clientv3.PutResponse, error) {
	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	resp, err := sc.Client.Put(ctx, key, value, opts...)
	cancel()
	return resp, err
}

func (sc *SimpleClient) Put(key string, value string) error {
	_, err := sc.put(key, value)
	return err
}

func (sc *SimpleClient) PutWithLease(key string, value string, leaseID clientv3.LeaseID) error {
	_, err := sc.put(key, value, clientv3.WithLease(leaseID))
	return err
}

func (sc *SimpleClient) get(key string, opts ...clientv3.OpOption) (*clientv3.GetResponse, error) {
	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	resp, err := sc.Client.Get(ctx, key, opts...)
	cancel()
	return resp, err
}

func (sc *SimpleClient) Get(key string) ([]byte, error) {
	resp, err := sc.get(key)
	if err != nil {
		return nil, err
	}
	if len(resp.Kvs) > 0 {
		return resp.Kvs[0].Value, nil
	}
	return nil, &ErrNotFoundKey{Key: key}
}

func (sc *SimpleClient) GetString(key string) (string, error) {
	resp, err := sc.get(key)
	if err != nil {
		return "", err
	}
	if len(resp.Kvs) > 0 {
		return string(resp.Kvs[0].Value), nil
	}
	return "", &ErrNotFoundKey{Key: key}
}

func (sc *SimpleClient) GetStrings(prefix string) (map[string]string, error) {
	kvs := map[string]string{}
	resp, err := sc.get(prefix, clientv3.WithPrefix())
	if err != nil {
		return kvs, err
	}
	for _, kv := range resp.Kvs {
		kvs[string(kv.Key)] = string(kv.Value)
	}
	return kvs, nil
}

func (sc *SimpleClient) delete_(key string, opts ...clientv3.OpOption) (*clientv3.DeleteResponse, error) {
	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	resp, err := sc.Client.Delete(ctx, key, opts...)
	cancel()
	return resp, err
}

func (sc *SimpleClient) Delete(key string) (int64, error) {
	resp, err := sc.delete_(key)
	if err != nil {
		return 0, err
	}
	return resp.Deleted, err
}

func (sc *SimpleClient) DeleteWithPrefix(prefix string) (int64, error) {
	resp, err := sc.delete_(prefix, clientv3.WithPrefix())
	if err != nil {
		return 0, err
	}
	return resp.Deleted, nil
}

func (sc *SimpleClient) Compact(rev int64, opts ...clientv3.CompactOption) (*clientv3.CompactResponse, error) {
	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	resp, err := sc.Client.Compact(ctx, rev, opts...)
	cancel()
	return resp, err
}

func (sc *SimpleClient) do(op clientv3.Op) (clientv3.OpResponse, error) {
	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	resp, err := sc.Client.Do(ctx, op)
	cancel()
	return resp, err
}

func (sc *SimpleClient) txn(ctx context.Context) clientv3.Txn {
	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	t := sc.Client.Txn(ctx)
	cancel()
	return t
}

func (sc *SimpleClient) watch(key string, opts ...clientv3.OpOption) clientv3.WatchChan {
	rch := sc.Client.Watch(context.Background(), key, opts...)
	return rch
}

func (sc *SimpleClient) Watch(key string) clientv3.WatchChan {
	return sc.watch(key)
}

func (sc *SimpleClient) WatchWithPrefix(prefix string) clientv3.WatchChan {
	return sc.watch(prefix, clientv3.WithPrefix())
}

type WatchCallback func(ev *clientv3.Event)

func WatchLoop(rch clientv3.WatchChan, cb WatchCallback, wg *sync.WaitGroup) {
	if wg != nil {
		wg.Done()
	}
	for {
		select {
		case resp := <-rch:
			for _, ev := range resp.Events {
				cb(ev)
			}
		}
	}
}
