package etcdv3

import (
	"context"
	"fmt"
	"os"
	"testing"
	"time"

	"github.com/coreos/etcd/clientv3"

	"gitee.com/nggs/util"
)

func Test_Base(t *testing.T) {
	endpoint := os.Getenv("ETCD_ENDPOINT")
	if endpoint == "" {
		t.Error("not found env ETCD_ENDPOINT or ETCD_ENDPOINT is empty string")
		return
	}

	t.Logf("endpoint=[%s]\n", endpoint)

	err := SC.Init(endpoint)
	if err != nil {
		t.Error(err)
		return
	}
	defer SC.Close()

	key := util.GenRandomString(16)
	value := util.GenRandomString(16)

	_, err = SC.put(key, value)
	if err != nil {
		t.Error(err)
		return
	}

	t.Logf("put key=[%s], value=[%s]\n", key, value)

	resp, err := SC.get(key)
	if err != nil {
		t.Error(err)
		return
	}
	for _, kv := range resp.Kvs {
		t.Logf("get key=[%s], value=[%s]\n", kv.Key, kv.Value)
	}

	if _, err := SC.delete_(key); err != nil {
		t.Error(err)
	} else {
		t.Logf("delete key=[%s]\n", key)
	}

}

func Test_GetWithRev(t *testing.T) {
	endpoint := os.Getenv("ETCD_ENDPOINT")
	if endpoint == "" {
		t.Error("not found env ETCD_ENDPOINT or ETCD_ENDPOINT is empty string")
		return
	}

	t.Logf("endpoint=[%s]\n", endpoint)

	err := SC.Init(endpoint)
	if err != nil {
		t.Error(err)
		return
	}
	defer SC.Close()

	presp, err := SC.Client.Put(context.TODO(), "foo", "bar1")
	if err != nil {
		t.Error(err)
		return
	}
	t.Log(presp.Header.Revision)
	_, err = SC.Client.Put(context.TODO(), "foo", "bar2")
	if err != nil {
		t.Error(err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	resp, err := SC.Client.Get(ctx, "foo", clientv3.WithRev(presp.Header.Revision))
	cancel()
	if err != nil {
		t.Error(err)
		return
	}
	for _, ev := range resp.Kvs {
		t.Logf("%s : %s\n", ev.Key, ev.Value)
	}
}

func Test_PutGetDelete(t *testing.T) {
	endpoint := os.Getenv("ETCD_ENDPOINT")
	if endpoint == "" {
		t.Error("not found env ETCD_ENDPOINT or ETCD_ENDPOINT is empty string")
		return
	}

	t.Logf("endpoint=[%s]\n", endpoint)

	err := SC.Init(endpoint)
	if err != nil {
		t.Error(err)
		return
	}
	defer SC.Close()

	begin := time.Now()

	k := "test:key:1:2:3"

	for i := 0; i < 1000; i++ {
		v := util.GenRandomString(8)

		err := SC.Put(k, v)

		v, err = SC.GetString(k)
		if err != nil {
			t.Error(err)
			return
		}

		//fmt.Println(v)

		_, err = SC.Delete(k)
		if err != nil {
			t.Error(err)
			return
		}

		//t.Log(n)
	}

	t.Logf("consume=%v\n", time.Now().Sub(begin))
}

func Test_PutGetDeleteWithPrefix(t *testing.T) {
	endpoint := os.Getenv("ETCD_ENDPOINT")
	if endpoint == "" {
		t.Error("not found env ETCD_ENDPOINT or ETCD_ENDPOINT is empty string")
		return
	}

	t.Logf("endpoint=[%s]\n", endpoint)

	err := SC.Init(endpoint)
	if err != nil {
		t.Error(err)
		return
	}
	defer SC.Close()

	for i := range make([]int, 3) {
		err := SC.Put(fmt.Sprintf("key_%d", i), fmt.Sprintf("value_%d", i))
		if err != nil {
			t.Error(err)
			return
		}
	}

	kvs, err := SC.GetStrings("key_")
	if err != nil {
		t.Error(err)
		return
	}
	fmt.Println(kvs)

	n, err := SC.DeleteWithPrefix("key_")
	if err != nil {
		t.Error(err)
		return
	}
	t.Log(n)
}

func Test_Watch(t *testing.T) {
	endpoint := os.Getenv("ETCD_ENDPOINT")
	if endpoint == "" {
		t.Error("not found env ETCD_ENDPOINT or ETCD_ENDPOINT is empty string")
		return
	}

	t.Logf("endpoint=[%s]\n", endpoint)

	err := SC.Init(endpoint)
	if err != nil {
		t.Error(err)
		return
	}
	defer SC.Close()

	k := "key"
	//v := "value"

	rch := SC.WatchWithPrefix(k)
	go WatchLoop(rch, func(ev *clientv3.Event) {
		t.Logf("%v\n", ev)
	}, nil)

	time.Sleep(1 * time.Second)

	err = SC.Put(k, util.GenRandomString(8))
	if err != nil {
		t.Error(err)
		return
	}

	_, err = SC.Delete(k)
	if err != nil {
		t.Error(err)
		return
	}

	time.Sleep(5 * time.Second)
}

func Test_PutWithLease(t *testing.T) {
	endpoint := os.Getenv("ETCD_ENDPOINT")
	if endpoint == "" {
		t.Error("not found env ETCD_ENDPOINT or ETCD_ENDPOINT is empty string")
		return
	}

	t.Logf("endpoint=[%s]\n", endpoint)

	err := SC.Init(endpoint)
	if err != nil {
		t.Error(err)
		return
	}
	defer SC.Close()

	leaseID, err := SC.GrantLease(5)
	if err != nil {
		t.Error(err)
		return
	}

	k := "key"
	v := "value"

	err = SC.PutWithLease(k, v, leaseID)
	if err != nil {
		t.Error(err)
		return
	}
}

func Test_KeepAlive(t *testing.T) {
	endpoint := os.Getenv("ETCD_ENDPOINT")
	if endpoint == "" {
		t.Error("not found env ETCD_ENDPOINT or ETCD_ENDPOINT is empty string")
		return
	}

	t.Logf("endpoint=[%s]\n", endpoint)

	err := SC.Init(endpoint)
	if err != nil {
		t.Error(err)
		return
	}
	defer SC.Close()

	k := "foo"
	v := "bar"

	leaseID, err := SC.GrantLease(5)

	err = SC.PutWithLease(k, v, leaseID)
	if err != nil {
		t.Error(err)
		return
	}

	// the key 'foo' will be kept forever
	ch, err := SC.KeepAliveLease(leaseID)
	if err != nil {
		t.Error(err)
		return
	}

	ka := <-ch
	t.Log("ttl:", ka.TTL)

	time.Sleep(5 * time.Second)
}

//func Test_KeepAliveOnce(t *testing.T) {
//	endpoint := os.Getenv("ETCD_ENDPOINT")
//	if endpoint == "" {
//		t.Error("not found env ETCD_ENDPOINT or ETCD_ENDPOINT is empty string")
//		return
//	}
//
//	fmt.Printf("endpoint=[%s]\n", endpoint)
//
//	Init(endpoint)
//	defer Close()
//
//	k := "foo"
//	v := "bar"
//
//	leaseID, err := GrantLease(5)
//
//	err = PutWithLease(k, v, leaseID)
//	if err != nil {
//		t.Error(err)
//		return
//	}
//
//	time.Sleep(4 * time.Second)
//
//	// to renew the lease only once
//	resp, err := KeepAliveLeaseOnce(leaseID)
//	if err != nil {
//		t.Error(err)
//		return
//	}
//
//	fmt.Println("ttl:", resp.TTL)
//}

func Test_Txn(t *testing.T) {
	endpoint := os.Getenv("ETCD_ENDPOINT")
	if endpoint == "" {
		t.Error("not found env ETCD_ENDPOINT or ETCD_ENDPOINT is empty string")
		return
	}

	t.Logf("endpoint=[%s]\n", endpoint)

	err := SC.Init(endpoint)
	if err != nil {
		t.Error(err)
		return
	}
	defer SC.Close()

	kvc := clientv3.NewKV(SC.Client)

	_, err = kvc.Put(context.TODO(), "key", "xyz")
	if err != nil {
		t.Error(err)
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), requestTimeout)
	_, err = kvc.Txn(ctx).
		If(clientv3.Compare(clientv3.Value("key"), ">", "abc")). // txn value comparisons are lexical
		Then(clientv3.OpPut("key", "XYZ")).                      // this runs, since 'xyz' > 'abc'
		Else(clientv3.OpPut("key", "ABC")).
		Commit()
	cancel()
	if err != nil {
		t.Error(err)
		return
	}

	resp, err := kvc.Get(context.TODO(), "key")
	cancel()
	if err != nil {
		t.Error(err)
		return
	}
	for _, ev := range resp.Kvs {
		t.Logf("%s : %s\n", ev.Key, ev.Value)
	}
	// Output: key : XYZ
}
