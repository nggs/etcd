package etcdv3

import "fmt"

type ErrNotFoundKey struct {
	Key string
}

func (e ErrNotFoundKey) Error() string {
	return fmt.Sprintf("not found key=[%s]", e.Key)
}
